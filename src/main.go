/*
A tool to measure the productivity in terms of programming for developers
*/

package main

import (
	"fmt"
)

func main() {
	// Print “Hello, World!” to console
	fmt.Println("Hello, World!")
}

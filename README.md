# go-pragmatic

A project to measure the productivity in terms of programming for developers.
This project will help to self-assess the efforts and progress in becoming a software developer.

Initial iteration is measuring the day-to-day Lines of Code(LoC) written.
As number of lines of code is not a valid productivity metrics anymore the project will concentrate on many factors that are intrinsic and the most essential.

#### Features:
    1. Measure everyday Lines of Code programmed
    2. Number of hours programmed
    3. Number of commits per branch per project
    4. Number of test files written
    5. Number of builds and tests passed
    6. Number of merge requests accepted without revert.

(Feature tend to get added/reduced/removed along with evaluation and evolution of the project)

### Author:
    Arvind sharma Murugan
    arvind4nitkkr@gmail.com
